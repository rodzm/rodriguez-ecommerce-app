import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';


import Cards from '../components/Cards';


import Carousel from '../components/Carousel';
import Highlights from '../components/Highlights';
import Slider from '../components/Slider';
import Hero from './Hero'

export default function Home(){

	return(
		
		<Fragment>
		    <Container fluid className='bg-danger mt-5 pb-3 text-center text-light lg-text bolder'>
       		 <p className='mt-5 pt-5'>Only Today! UP TO 50% OFF! SHOP NOW!</p>
    		</Container>
				<Fade>
		<Zoom>
			<Carousel />
				</Zoom>
				</Fade>

			<Highlights />

	

			<Slider/>
			<Fade bottom>

			<Hero />
			</Fade>

			<Fade bottom>
				<Cards></Cards>
			</Fade>
		</Fragment>
	)
}