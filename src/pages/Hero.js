import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';

export default function Error(){
	const data = {
		title: 'SIGN UP & SAVE',
		content: 'SAVE 15% when you sign up for exclusive Geek Freaks offers and content!',
		destination: '/'
	}

	return(
		<Container fluid className='bg-secondary'>

		<Banner data={data}/>
		
		</Container>
	)
}