import { Fragment, useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useParams } from "react-router-dom";

import OrderItem from "../components/OrderItem";

export default function OrderConfirmation(params) {

 
  const { orderId } = useParams();

  const [products, setProducts] = useState([]);
  const [date, setDate] = useState([]);
  const [totalAmount, setTotalAmount] = useState([]);

  const orderNo = (orderId.replace(/\D/g, "")).slice(0,7);



  useEffect(()=> { 
    
		fetch(`${ process.env.REACT_APP_API_URL }/orders/${orderId}`, {
      method: "GET",
      headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
    .then(res => res.json())
		.then(res=>{
      console.log(res)
      const order = res.products
      setDate(res.purchasedOn)
      setTotalAmount(res.totalAmount)
      setProducts(order.map(res => {
        return (
          <OrderItem key={res.productId} productProp={res} />
          
        )
      }))
    })

	},[])

  console.log(date)
    return (
        <Fragment>
        <Container>
          <Row>
            <Col lg={3}>

            </Col>
            <Col lg={6}>

            <Container className='text-center mt-5 pt-2'>
            <h1 className='title-text bolder mt-5 pt-5'>Order placed! Thank you!</h1>
            <p className='reg-text'> Confirmation will be sent to your email.</p>
            </Container>
            <Container >

			<p className='lg-text text-center bold ms-4'>Order Summary:</p>

      <Container className='mb-3 menu-text py-3 bg-secondary'>
      <Row><Col className='bold col-6 left'> Order No: </Col>{orderNo}</Row>
      <Row><Col className='bold col-6 left'> Total Amount: </Col>$ {totalAmount} </Row>

      {/* <Row><Col className='bold col-6 left'> Order Date: </Col>{orderDate} </Row> */}
      </Container>

      <p className='lg-text center bold'>Order Items:</p>
            <Container className='m-0 p-0  shadow'>
              {products}
            </Container>
      </Container>

            </Col>
            <Col lg={3}>

            </Col>
          </Row>
        </Container>





        </Fragment>
    )
  
};
