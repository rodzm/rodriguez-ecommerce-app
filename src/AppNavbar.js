import { Fragment, useContext, useState } from "react";

import { Link, NavLink } from 'react-router-dom';

import UserContext from './UserContext';

import {Navbar, Nav, Container, Row, Col, Offcanvas, Stack } from 'react-bootstrap';

import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

import { Turn as Hamburger } from 'hamburger-react'

import Cart from "../src/components/Cart"
import { Button } from "@mui/material";


export default function AppNavbar(){


	const { user } = useContext(UserContext);

  console.log(user)

	const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const adminKey = () => {
    if (localStorage.admin === "true") {
      return true;
    } else {
      return false;
    }
  }

  const adminData = adminKey()
  console.log(adminData)

	return(
<>
  <Container fluid  className='text-secondary mb-3 pb-5'>
  <Row>
  <Navbar bg="dark" id='navbar' variant='dark' expand="lg" fixed='top'>

  {/* LEFT BAR MOBILE*/}
  <Nav className='navflex-start my-1 py-0 col-4 d-lg-none'>
          <Nav.Link className='my-0 py-0'>
              <Navbar.Toggle className='my-0 py-0 border-0 text-light'>
                  <Hamburger rounded size={21}/>
              </Navbar.Toggle>
          </Nav.Link>
  </Nav>

  {/* LOGO */}
  <Col className='navflex-center col-2 d-none d-lg-block ps-5'>
      <Link to={'/'}>
          <img src="https://i.imgur.com/9ZbpZTK.png"
          alt='logo'
          style={{width: 200, height: "auto"}}
          className="d-inline-block align-top"/>
      </Link>
  </Col>

  {/* MOBILE LOGO */}
  <Col className='navflex-center col-4 d-lg-none'>
      <Nav.Link href='/'>
          <img src="https://i.imgur.com/9ZbpZTK.png"
          alt='logo'
          style={{width: 200, height: "auto"}}
          className="d-inline-block align-top"/>
      </Nav.Link>
  </Col>

  {/* RIGHT BAR MOBILE */}
  <Col className='col-4 d-lg-none'>
      <Container className='navflex-end'>
          <Stack className='me-2' direction='horizontal' gap='3'>
              <Nav.Link className='nav-link text-light'>
                  <AccountCircleOutlinedIcon sx={{fontSize: "2.5rem"}}/>
              </Nav.Link>
              <Nav.Link className='nav-link text-light' onClick={handleShow}>
                  <ShoppingCartOutlinedIcon sx={{fontSize: "2.5rem"}}/>
              </Nav.Link>
          </Stack>
      </Container>
  </Col>
  {/* RIGHT MENU  */}
  <Col className='col-10 d-none d-lg-block' id='navfix'>
      <Container className='navflex-end'>
          <Stack className='text-light mx-3' direction='horizontal' gap='5'>

                {( adminData === true ) ?
                <>
                <NavLink id='textlink' to={'/dashboard'}>Dashboard</NavLink>
                <NavLink id='textlink' to={'/products'}>Products</NavLink>
                </>
              :
                <>
                <NavLink id='textlink' to={'/products'}>Products</NavLink>
                </>
                }

                {(localStorage.email === undefined) ?
                <>
                <NavLink id='textlink' to={'/login'}>Login</NavLink>
                <NavLink id='textlink' to={'/register'}>Register</NavLink>
                </>

              :
                <>
                <NavLink id='textlink' to={'/account'}>Account</NavLink>
                <NavLink id='textlink' to={'/logout'}>Logout</NavLink>
                </>
                }

                {( adminData === false ) ?
                <Nav.Link onClick={handleShow}>Cart</Nav.Link>
                :
                <></>
                }
          </Stack>
      </Container>
  </Col>

<container className='navflex-start d-lg-none'>
      <Navbar.Collapse>

  {/* SEARCHBOX */}
      {/* <Form className="d-flex px-3">
        <Container>
        <Row>
                  <Col className='col-1'>
                    <SearchIcon />
                  </Col>
                  <Col className='col-8'>
                  <Form.Control 
                    type="search"
                    id='searchbox'
                    // placeholder={{SearchIcon}}
                    aria-label="Search"
                    ></Form.Control>
                  </Col>
          </Row>
          </Container>
      </Form> */}

          <Nav className="collapseMenu px-4 pb-4 pt-3">
          {(localStorage.isAdmin === true) ?
            <NavLink id='textlink'  to={'/dashboard'}>Dashboard</NavLink>
          :
          <></>
          }
              <NavLink id='textlink'  to={'/products'}>Products</NavLink>
              <NavLink id='textlink'  to={'/products'}>New Arrivals</NavLink>
              <NavLink id='textlink'  to={'/products'}>Brands</NavLink>
              <NavLink id='textlink'  to={'/account'}>Account</NavLink>
              <NavLink id='textlink'  to={'/logout'}>Logout</NavLink>
          </Nav>
      </Navbar.Collapse>

</container>
  </Navbar>
  </Row>
  </Container>

  <Offcanvas scroll="false"  className="no-scroll" id='offcanvas' show={show} onHide={handleClose} placement="end">
      <Offcanvas.Header >
      {(localStorage.email !== undefined) ?
        <>
            <Container>
              <Offcanvas.Title className='title-text ms-3 mt-5 mb-3 bolder'>My Cart</Offcanvas.Title>
            </Container>
        </>
        :
        <>
          <Offcanvas.Title className='title-text ms-3 mt-5 mb-3 bolder'>Log in or create an account</Offcanvas.Title>
        </>
      }
      </Offcanvas.Header>

      {(localStorage.email === undefined) ?
  <Fragment>
      <Offcanvas.Body>
            <ul>
              <li>for faster checkout</li>
              <li>to unlock personalised offers</li>
              <li>access order history and track deliveries</li>
            </ul>
            <hr className="solid mt-5" />

              <Container><Nav.Link className='link-oc' href={"/login"}>
                <AccountCircleOutlinedIcon className='mx-3'/>Log in</Nav.Link>
              </Container>

            <hr className="solid" />

              <Container><Nav.Link className='link-oc' href={"/register"}>
                <PersonAddAltOutlinedIcon  className='mx-3'/>Create an Account</Nav.Link>
              </Container>

            <hr className="solid" />

              <Container><Nav.Link className='link-oc'>
                <LocalShippingOutlinedIcon  className='mx-3'/>Track Orders</Nav.Link>
              </Container>

            <hr className="solid" />

              <Container><Nav.Link className='link-oc'>
                <ChatOutlinedIcon  className='mx-3'/>Contact Us</Nav.Link>
              </Container>
      </Offcanvas.Body> 
    </Fragment>  
    :
    <>
      <Cart style={{overflowY: "scroll"}} />
      <Container className='center mt-0'>
        <Container className='mt-0'>
      <Button id="offcanvas-center-btn" className='ms-2 mt-0' onClick={handleClose} ><Link to="/products" id="btn-link-rrd">Continue Shopping</Link></Button>

        </Container>
      </Container>
    </>
      }
  </Offcanvas>
</>
		)
}