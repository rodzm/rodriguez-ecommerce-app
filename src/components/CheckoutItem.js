import { useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

export default function CartItem({productProp}) {
    const {productId, name, price, quantity, img}  = productProp

    const [ qty, setQty ] = useState(0);

    const increase = () => {
       setQty(qty => qty + 1);
    };
 
    const decrease = () => {
        if (qty+quantity > 0) {
            setQty(qty => qty - 1);
        }
    };

    return (
        <Container className='my-5 me-5 reg-text' style={{ width: 390, height: 'auto' }}>
        <Card.Body >
        <Container>
            <Row>
                <Col xs={3}>
                    <Card.Img src={img} style={{ width: 'auto', height: 80 }}/>
                </Col>
                <Col xs={9} className='pt-3'>
                    <Card.Title className='lg-text bolder pt-2 ps-5'>{name}</Card.Title>
                    <Container>
                        <Row>
                            <Col>
                                 <Card.Text className='lg-text pt-3 ps-5'>x {quantity}</Card.Text>   
                            </Col>
                            <Col>
                                  <Card.Text className='lg-text pt-3 ps-5'>$ {((qty+quantity)*price).toFixed(2)}</Card.Text>
                            </Col>
                        </Row>
                    </Container>

                </Col>
            </Row>
        </Container>             
        </Card.Body>
        </Container>
    )
}
