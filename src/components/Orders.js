import React, { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';

import OrderCard from './OrderCard';

export default function Orders () {

  const [ orders, setOrders ] = useState([]);
	const token = localStorage.token

	useEffect(()=> { 
	fetch('http://localhost:4000/users/myOrders', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(res=>{
			setOrders(res.map(order=>{
					return(
					<OrderCard key={order._id} orderProp={order} />
					)
			}))
		})
	},[])
	console.log(orders.length)

  return (
		<Container className='mx-md-5 mt-md-3 pe-md-5'>
			<p className='menu-text bold ms-4'>Orders</p>
        <Row className='ms-1 col-10 pe-md-5'>

		{(orders.length !== 0) ?
		<>
            {orders}
						</>
		:
		<p className='menu-text'>You have not placed any orders yet.</p>
		}
        </Row>
      </Container>


  )
}