import { useState, useEffect } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import CartItem from './CartItem';

import Swal from 'sweetalert2';
import { Button } from '@mui/material';

export default function Cart() {

  const navigate = useNavigate();

  const [products, setProducts] = useState([]);
  const [products2, setProducts2] = useState([]);
  const token = localStorage.token;

  useEffect(()=>{
    fetch(`${ process.env.REACT_APP_API_URL }/cart`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
				Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json()) 
    .then(res => {
      setProducts2(res.map(info => {
        const data = info.products
        return(
          setProducts(data.map(res => {
              return(
              <CartItem key={res.productId} productProp={res} />
        
               )
          }))
        );
      }));
    })
  },[products])

  const checkOut = () => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/checkOut`, {
      method: "POST",
      headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
    .then(res=> res.json())
    .then(data => {
			if (data === true) {
        Swal.fire({
					title: "Item added to cart",
					icon: "success",
				});
                
      } else {
        Swal.fire({
          title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
      }
    })
  }

  return (<>
               {(products.length !== 0) ?
						<Container className='scroll mb-0'>
								<Row className=''>
									<Col className=''>
         
									{products}
     
									</Col> 
                  <Container className='center ps-1 mt-5'>
                  <Button className='ms-3 center mt-3 col-6 mb-5' id='btn' href={'/checkout'} block>
                    Check Out
                  </Button>

                  </Container>
								</Row>
						</Container>
            :
            <Container className='center'>
								<Row className='mt-5 pt-5'>
									<Col className=''>
         
									<p className='menu-text'>Your cart is empty.</p>
     
									</Col> 
								</Row>
						</Container>
               }
    </>
    )
}
