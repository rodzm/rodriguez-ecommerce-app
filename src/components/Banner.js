import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	const { title, content, destination, label } = data;
	return(
			<Row>
				<Col className="p-5">
					<h1 className='title-text bolder p-5 mx-5'>{title}</h1>
					<p className='lg-text mx-5 px-5'>{content}</p>
					<Link to={destination} className='ms-5 ps-5 lg-text bolder'>{label}</Link>
				</Col>
			</Row>
		)
}