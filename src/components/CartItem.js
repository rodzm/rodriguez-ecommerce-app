import { useState } from 'react';
import { Container, Button, Row, Col, InputGroup, Form, Card, Nav } from 'react-bootstrap';

import ClearRoundedIcon from '@mui/icons-material/ClearRounded';

export default function CartItem({productProp}) {
    
    const {productId, name, price, quantity, img}  = productProp

    const [ qty, setQty ] = useState(0);

    const increase = () => {
       setQty(qty => qty + 1);
    };
 
    const decrease = () => {
        if (qty+quantity > 0) {
            setQty(qty => qty - 1);
        }
    };

    const removeFromCart = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/cart/remove`, {
            method: 'POST',
            headers: {
              'Content-type': 'application/json',
                      Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
            })
          })
          .then(res => res.json()) 
          .then(res => {
          })
    }


    return (
        <Card className='reg-text mb-1 ps-5 ms-4' style={{ width: 460, height: 'auto' }}>
        <Card.Body >
        <Container>
            <Row>
                <Col xs={3}>
                    <Card.Img src={img} style={{ width: 'auto', height: 100 }}/>
                </Col>
                <Col xs={9} className=''>
                    <Card.Title className='lg-text bolder pt-2 ps-5'>{name}</Card.Title>
                    <Card.Text className='lg-text pt-2 ps-5'>$ {((qty+quantity)*price).toFixed(2)}</Card.Text>
                    {/* <Counter /> */}
                    <Container>
                        <Row>
                            <Col xs={6}>
                            <InputGroup className="mb-2 pt-1 px-0 ps-5" style={{width: '15rem'}}>
                        <Button variant='light' className='counter-btn border' onClick={increase}> + </Button>
                            <Form.Control 
                            small
                            className='reg-text text-center counter'
                            value={qty+quantity}
                            onChange={e=> setQty(e.target.value)} 
                            />
                        <Button variant='light' className='counter-btn border' onClick={decrease}> - </Button>
                    </InputGroup>
                                
                            </Col>
                            <Col xs={6}>
                            <Button id='inactive-btn' className='ms-5 mt-2' onClick={()=>removeFromCart()}><ClearRoundedIcon /></Button>


                            </Col>
                        </Row>
                    </Container>
                  
                </Col>
            </Row>
        </Container>             
        </Card.Body>
        </Card>
    )
}
