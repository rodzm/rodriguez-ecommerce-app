import React from 'react'
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UserList = ({userProp }) => {

    const { firstName, lastName, email, isAdmin, _id } = userProp;

    const setAdmin = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/setAdmin`,{
			 method: "PUT",
		 headers: {
			 'Content-Type': 'application/json',
				 Authorization: `Bearer ${localStorage.getItem('token')}`
			 },
			 body: JSON.stringify({
					id: _id,
					isAdmin: isAdmin,
			 })
		})
		.then(res => res.json())
		.then(data => {
			 if (data === true) {
			 } else {
					Swal.fire({
						 title: "Something went wrong.",
						 icon: "error",
					})
			 }
		})
	}


    return (

        <tr key={_id}>
            <td>{firstName}</td>
            <td>{lastName}</td>
            <td>{email}</td>
            <td>{_id}</td>
            <td>
            {isAdmin ?
							<Button id='active-btn' onClick={()=>setAdmin()}>ADMIN</Button>
				:
            	<Button id='inactive-btn' onClick={()=>setAdmin()} >Non-Admin</Button>
						}
            </td>

        </tr>


  )
}

export default UserList