import { useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

export default function OrderItem({productProp}) {
    const {productId, name, price, quantity, img}  = productProp

    return (
        <Container className='mb-3 px-5 py-2 reg-text' style={{ width: 480, height: 'auto' }}>
        <Card.Body >
        <Container>
            <Row>
                <Col xs={3}>
                    <img src={img} style={{ width: 'auto', height: 100 }}/>
                </Col>
                <Col xs={9} className='pt-3'>
                    <Card.Title className='lg-text pt-2 ps-5'>{name}</Card.Title>
                    <Container>
                        <Row>
                            <Col>
                                 <Card.Text className='lg-text pt-3 ps-5'>x {quantity}</Card.Text>   
                            </Col>
                            <Col>
                                  <Card.Text className='lg-text pt-3 ps-5'>$ {((quantity)*price).toFixed(2)}</Card.Text>
                            </Col>
                        </Row>
                    </Container>

                </Col>
            </Row>
        </Container>             
        </Card.Body>
        </Container>
    )
}
