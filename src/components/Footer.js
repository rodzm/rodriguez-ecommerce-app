import { Row, Col, Container, Nav, Stack} from 'react-bootstrap';


import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Prefooter from './Prefooter';

export default function Footer(){
	return(<>
		<Prefooter ></Prefooter>
		<Container id='footer' fluid className='footer bg-darker text-lite text-left px-lg-5 pt-5'>
    <Accordion disableGutters className='d-lg-none' sx={{backgroundColor: '#222222', color:'#cccccc', boxShadow: "none", '&:after': {
                display: 'none',
            }}}>
        <AccordionSummary >
          <h1 id='footer2'><strong>ACCOUNT</strong></h1>
        </AccordionSummary>
        <AccordionDetails id='footer2' className='text-lite'>
        <Nav.Link href='#'>Create account</Nav.Link>
				<Nav.Link href='#'>Sign in</Nav.Link>
				<Nav.Link href='#'>Wishlist</Nav.Link>
				</AccordionDetails>
		</Accordion>
		<Accordion disableGutters className='d-lg-none' sx={{backgroundColor: '#222222', color:'#cccccc', boxShadow: "none"}}>
        <AccordionSummary>
          <h1 id='footer2'><strong>RESOURCES</strong></h1>
        </AccordionSummary>
        <AccordionDetails id='footer2' className='text-lite'>
				<Nav.Link href='#'>Shipping Policy</Nav.Link>
							<Nav.Link href='#'>Terms & Conditions</Nav.Link>
							<Nav.Link href='#'>Privacy & terms</Nav.Link>
				</AccordionDetails>
		</Accordion>

		<Accordion disableGutters className='d-lg-none' sx={{backgroundColor: '#222222', color:'#cccccc', boxShadow: "none"}}>
        <AccordionSummary>
          <h1 id='footer2'><strong>COMPANY</strong></h1>
        </AccordionSummary>
        <AccordionDetails id='footer2' className='text-lite'>
				<Nav.Link href='#'>About Us</Nav.Link>
						<Nav.Link href='#'>FAQs</Nav.Link>
						<Nav.Link href='#'>Contact Us</Nav.Link>
				</AccordionDetails>
		</Accordion>

		<Accordion disableGutters className='d-lg-none' sx={{backgroundColor: '#222222', color:'#cccccc', boxShadow: "none"}}>
        <AccordionSummary>
          <h1 id='footer2'><strong>CONTACT US</strong></h1>
        </AccordionSummary>
        <AccordionDetails id='footer2' className='text-lite'>
        <Nav.Link href='#'>623 Harrison St., 2nd Floor 
						San Francisco, CA 94107</Nav.Link>
				<Nav.Link href='#'>415-201-6370</Nav.Link>
				<Nav.Link href='#'>hello@geekfreaks.com</Nav.Link>
				</AccordionDetails>
		</Accordion>
    
			<Row className='justify-content-center px-4 pb-4 px-lg-5'>
						
            <Col md={2} className='pt-3 d-none d-lg-block'>
						<h1 id='footer2'><strong>ACCOUNT</strong></h1>
						<Nav.Link href='#'>Create account</Nav.Link>
						<Nav.Link href='#'>Sign in</Nav.Link>
						<Nav.Link href='#'>Wishlist</Nav.Link>
						</Col>

						<Col md={2} className='pt-3 d-none d-lg-block'>
						<h1 id='footer2'><strong>COMPANY</strong></h1>
						<Nav.Link href='#'>About Us</Nav.Link>
						<Nav.Link href='#'>FAQs</Nav.Link>
						<Nav.Link href='#'>Contact Us</Nav.Link>
						</Col>

						<Col md={2} className='pt-3 d-none d-lg-block'>
						<h1 id='footer2'><strong>RESOURCES</strong></h1>
							<Nav.Link href='#'>Shipping Policy</Nav.Link>
							<Nav.Link href='#'>Terms & Conditions</Nav.Link>
							<Nav.Link href='#'>Privacy & terms</Nav.Link>
						</Col>

						<Col md={3} className='order-md-first pt-3'>
						<img src='https://i.imgur.com/9ZbpZTK.png' style={{width: 200, height: "auto"}}></img>
						<Stack gap='2' direction='horizontal' className='opacity-3'>
						<Nav.Link href='https://www.facebook.com'><FacebookIcon  sx={{fontSize: "2.5rem"}}/></Nav.Link>
						<Nav.Link href='https://www.instagram.com'><InstagramIcon  sx={{fontSize: "2.5rem"}}/></Nav.Link>
						<Nav.Link href='https://www.twitter.com'><TwitterIcon  sx={{fontSize: "2.5rem"}}/></Nav.Link>

						</Stack><br></br>
						Copyright © 2022 by Geek Freaks, Inc.
						<br></br>All rights reserved.
						</Col>

						<Col md={3} xs={12} className='order-md-first pt-3 d-none d-lg-block'>
						<h1 id='footer2'><strong>CONTACT US</strong></h1>
						623 Harrison St., 2nd Floor<br></br>
						San Francisco, CA 94107
						<br></br>
						<br></br>
						415-201-6370
						<br></br>
						hello@geekfreaks.com		


           </Col>
			</Row>

		</Container>
    </>
		)
}






     

