import React, { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';

import AccountCard from './AccountCard';

export default function Accountinfo () {

  const [ account, setAccount ] = useState([]);
	const token = localStorage.token

	useEffect(()=> { 
	fetch('http://localhost:4000/users/details', {
			headers:{ Authorization: `Bearer ${token}`}
		})
		.then(res => res.json())
		.then(user=>{
        setAccount({user})
				return(
						<AccountCard key={user._id} accountProp={user} />
				)
		})
	},[])


  return (
      <Container>
        <Row className='col-12'>
            {account}
        </Row>
      </Container>
  )
}