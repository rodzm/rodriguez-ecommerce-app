import { Button } from '@mui/material'
import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'

const Cards = () => {
  return (
    <Container className='my-5 py-5'>
      <Row>
        <Col md={6} className='text-center'>
        <Card.Img variant="top" className='bg-info' style={{ width: '500px', height: 'auto'}} src="https://static.thcdn.com/images/small/webp/widgets/96-en/15/Product-580x384-030515.jpg" />
        <Card.Body>
      <Card.Title className='menu-text bolder pt-3' >AWESOME COOL TSHIRTS</Card.Title>
      <Card.Text>
      </Card.Text>
      <Button id='btn' variant="primary">GRAB YOURS NOW</Button>
    </Card.Body>
        

        </Col>
        <Col md={6} className='text-center'>

        <Card.Img variant="top" className='bg-info' style={{ width: '500px', height: 'auto'}} src="https://static.thcdn.com/images/small/webp/widgets/96-en/15/Product-580x384-030515.jpg" />
        <Card.Body>
      <Card.Title className='menu-text bolder pt-3' >AWESOME COOL TSHIRTS</Card.Title>
      <Card.Text>
      </Card.Text>
      <Button id='btn' variant="primary">GRAB YOURS NOW</Button>
    </Card.Body>

        </Col>
      </Row>
    </Container>
  )
}

export default Cards