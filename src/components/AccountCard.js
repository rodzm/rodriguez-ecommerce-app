import { Button } from '@mui/material';
import { Card, Col, Row } from 'react-bootstrap';

export default function AccountCard({accountProp}) {

   const {_id, email, firstName, lastName }  = accountProp;
   
   return (
           <Card className='m-1 reg-text' sx={{ p: 2, border: '1px solid lightgrey' }} style={{ width: "90vw", height:200 }}>
                <Card.Body >
                <Row>
                    <Col xs='6'>
                      <p>Order No:</p>
                        <Card.Text className='bold'>{email}</Card.Text>
                    </Col>
                    <Col xs='6'>
                      <p>Purchase Date:</p>
                      <Card.Text className='bold'>{firstName}</Card.Text>
                    </Col>
                </Row>
                <Row className='pt-4'>
                    <Col xs='6'>
                      <p>Status</p>
                    </Col>
                    <Col xs='6'>
                      <p>Order Amount</p>
                      <Card.Text className='bold'>PHP {lastName}</Card.Text>
                    </Col>
                </Row>
                <Row className='pt-4'>
                  <Button className='reg-text'> View Order Details</Button>
                </Row>
                </Card.Body>
            </Card>
   
         
   )
}
