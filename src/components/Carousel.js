import { Carousel } from 'react-bootstrap';

function CarouselUncontrolled() {
  return (<>
    <Carousel fluid className=' bg-dark slick-track'>
          <Carousel.Item>
            <img
              className="slick-slide d-block w-100 mx-auto d-none d-md-block"
              src="https://i.imgur.com/6yumcJk.png" 
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://i.imgur.com/BU95BjG.png"
              alt="slide-mobile"
            />
            <Carousel.Caption className='carousel-caption'></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="slick-slide d-block w-100 mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/22/original-0824-z-STDCRE-38758-z-jb-lotr-1920x586-1-080722.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/36/original-0824-z-STDCRE-38758-z-jb-lotr-750x563-1-080736.jpg"
              alt="slide-mobile"
            />
            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="slick-slide d-block w-100 mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/17/original-0824-STDCRE-40591-AP-Z-THOR-LOVE-THUNDER-MAIN-1920x586-080217.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/23/original-0824-STDCRE-40591-AP-Z-THOR-LOVE-THUNDER-MAIN-750x563-080223.jpg"
              alt="slide-mobile"
            />
            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
  
        </Carousel>
    
        </>);
}

export default CarouselUncontrolled;